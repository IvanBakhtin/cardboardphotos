package se.alphamou.cardboardphotos;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;

public class OneEyeView extends ViewGroup {
    private final ImageView _imageView;

    public OneEyeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _imageView = new ImageView(context, attrs);
        _imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        _imageView.setAdjustViewBounds(true);
        addView(_imageView);

        _imageView.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    public void setBitmap(Bitmap bitmap) {
        _imageView.setImageBitmap(bitmap);
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int width = right - left;
        int height = bottom - top;
        double imageSize = 0.9;

        double imageMargin = (1.0 - imageSize) / 2;
        double leftMargin = (int) (width * imageMargin);
        double topMargin = (int) (height * imageMargin);
        _imageView.layout(
                (int) leftMargin, (int) topMargin,
                (int) (leftMargin + width * imageSize), (int) (topMargin + height * imageSize));
    }
}

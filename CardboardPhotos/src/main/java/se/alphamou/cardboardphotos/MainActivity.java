package se.alphamou.cardboardphotos;

import com.google.vrtoolkit.cardboard.CardboardActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;

public class MainActivity extends CardboardActivity {

  private Vibrator _vibrator;
  private ImagePairView _imagePairView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.common_ui);

    _vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    _imagePairView = (ImagePairView) findViewById(R.id.overlay);

  }

  @Override
  public void onCardboardTrigger() {
    _vibrator.vibrate(50);
    _imagePairView.switchImages();
  }

}

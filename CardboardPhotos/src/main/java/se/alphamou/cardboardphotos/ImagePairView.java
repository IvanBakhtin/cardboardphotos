package se.alphamou.cardboardphotos;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImagePairView extends LinearLayout {
    private final OneEyeView _leftView;
    private final OneEyeView _rightView;

    private int _currentImgIndex = 0;
    private final boolean _cameraMode;
    private List<String> _images;
    private List<String> _left_images;
    private List<String> _right_images;
    private Pair<Bitmap, Bitmap> _next = null;

    public ImagePairView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
        params.setMargins(0, 0, 0, 0);

        _leftView = new OneEyeView(context, attrs);
        _leftView.setLayoutParams(params);
        addView(_leftView);

        _rightView = new OneEyeView(context, attrs);
        _rightView.setLayoutParams(params);
        addView(_rightView);

        //if folder /CardboardPhotos is found and has photos - show them
        //two subfolders are expected to be found there /left and /right
        //with the same number of photos,
        getPairedImages(context);
        if (_left_images.size() != 0) {
            _cameraMode = false;
        } else {
            _cameraMode = true;
            getCameraImages(context);
        }
        startLoadingImages(_currentImgIndex);
    }

    public void switchImages() {
        _currentImgIndex = getNextIndex(_currentImgIndex);
        setImages();
    }


    private void setImages() {
        if (_next != null) {
            _leftView.setBitmap(_next.first);
            _rightView.setBitmap(_next.last);

            _next = null;
        }
        startLoadingImages(getNextIndex(_currentImgIndex));
    }

    private void startLoadingImages(int index){
        new BitmapWorkerTask(index).execute(getImageNames(index));
    }

    private int getNextIndex(int index) {
        int result = index + 1;
        result %= _cameraMode ? _images.size() / 2 : _left_images.size();
        return result;
    }

    private Pair<String, String> getImageNames(int index) {
        if (_cameraMode)
            return new Pair(_images.get(2 * index), _images.get(2 * index + 1));
        else
            return new Pair(_left_images.get(index), _right_images.get(index));
    }


    private void getCameraImages(Context context) {
        _images = getImages(context, CAMERA_IMAGE_PATH);
        Collections.sort(_images);
        Collections.reverse(_images);
    }

    private void getPairedImages(Context context) {
        _left_images = getImages(context, PAIRED_LEFT_PATH);
        _right_images = getImages(context, PAIRED_RIGHT_PATH);

        Collections.sort(_left_images);
        Collections.sort(_right_images);
    }


    private static final String CAMERA_IMAGE_PATH = "/DCIM/Camera";

    private static final String PAIRED_LEFT_PATH = "/CardboardPhotos/left";
    private static final String PAIRED_RIGHT_PATH = "/CardboardPhotos/right";

    private static List<String> getImages(Context context, String rootedPath) {
        File sdCardRoot = Environment.getExternalStorageDirectory();
        File dir = new File(sdCardRoot, rootedPath);
        List<String> result = new ArrayList();
        if (dir.exists())
            for (File f : dir.listFiles())
                if (f.isFile()) {
                    String path = f.getAbsolutePath().toLowerCase();
                    if (path.endsWith(".jpg") || path.endsWith(".jpeg"))
                        result.add(path);
                }

        return result;
    }

    class BitmapWorkerTask extends AsyncTask<Pair<String, String>, Void, Pair<Bitmap, Bitmap>> {

        private final int _index;

        public BitmapWorkerTask(int index) {
            _index = index;
        }

        @Override
        protected Pair<Bitmap, Bitmap> doInBackground(Pair<String, String>... paths) {
            String left = paths[0].first;
            String right = paths[0].last;
            return new Pair(
                    decodeSampledBitmapFromFile(left, 1000, 1000),
                    decodeSampledBitmapFromFile(right, 1000, 1000));
        }

        @Override
        protected void onPostExecute(Pair<Bitmap, Bitmap> bitmaps) {
            boolean isCurrentlyNeeded = _index == _currentImgIndex;
            boolean isForNext = _index == getNextIndex(_currentImgIndex);
            if (isCurrentlyNeeded || isForNext)
                _next = bitmaps;
            if (isCurrentlyNeeded)
                setImages();
        }

        private int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        private Bitmap decodeSampledBitmapFromFile(String imagePath,
                                                   int reqWidth, int reqHeight) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imagePath, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(imagePath, options);
        }
    }

}

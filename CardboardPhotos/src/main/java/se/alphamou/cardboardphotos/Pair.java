package se.alphamou.cardboardphotos;

public class Pair<X, Y> {
    public final X first;
    public final Y last;

    public Pair(X first, Y last) {
        this.first = first;
        this.last = last;
    }
}

# CardboardPhotos #

3d photo viewer for an android phone using [google cardboard](https://www.google.com/get/cardboard/).

### How to make 3d photos? ###

In order to view photos in 3d you need to take a pair of photos of the same scene from two close positions. As if your eyes were able to take photos. I've found two ways to do that.

1. Take two photos with the phone (first right then left, just by convention), shifting phone parallel to itself between shots. At least vertical orientation should be as similar as possible. The app then will show these two photos side by side. Using cardboard magnet "button" will switch to the next pair of photos. The downside of this approach it that you'll only be able to take shots of static scenes, as you are not taking both photos at the same moment.
2. Use two cameras. Position them side by side and take two shots simultaneously. The app will look for photos in `CardboardPhotos/left` and `CardboardPhotos/right` folders on the external card. (`CardboardPhotos` will be on the same level as `DCIM` for example). If there is no external card in the phone.. well, I don't know, the app will crash probably. If there is no such folder or no photos in it, the app will display phone camera photos instead (see p.1). There are example photos in the repository and also [here](http://alphamou.se/eternal/CardboardPhotos.zip); just unzip and copy to the phone. The downside of two cameras approach is that you'll probably need tripods for them or some other support. I've found that it's impossible to direct cameras parallel just by holding them in hands. I had to post-edit photos rotating and cropping them to get acceptable pairs. Here is an example:  
![left](http://alphamou.se/eternal/DSC_0375.JPG left) ![right](http://alphamou.se/eternal/DSC_0195.JPG right)  
The pictures are not the same as is obvious from the colors. What I've found is that hues, white balance and such don't matter much for viewing. Positioning is crucial though.

### How to install the app? ###

Again, there are two ways.  

* Clone the repository. Build the code and install the result on your phone. I've used [android studio](http://developer.android.com/tools/studio/index.html) to develop it
* If you trust me, you can download [.apk from my site](http://alphamou.se/eternal/CardboardPhotos.apk) and install it. You'd need to enable installing apps not originating in android market in you phone settings.

### Disclaimer ###

I'm not an Android of Java developer actually. The code might not follow Java conventions. Though the worst that can happen, it'll crash.

### License ###

This app is licensed under [WTFPL](http://www.wtfpl.net/), namely, you are free to do whatever you wish.